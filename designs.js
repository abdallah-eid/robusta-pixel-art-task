const myColor = document.getElementById("colorPicker");
const height = document.getElementById("inputHeight");
const width = document.getElementById("inputWidth");
const grid = document.getElementById("pixelCanvas");

// When size is submitted by the user, call makeGrid()
// Drawing Grid after submission
document.getElementById("sizePicker").addEventListener("submit", (e) => {
  e.preventDefault()
  makeGrid();
});

function makeGrid() {
  // Your code goes here!
  // prevent appending grids over each other
  // console.log(grid.childNodes.length)
  if (grid.childNodes.length >= 1) {
    grid.innerHTML = "";
  }
  let isMouseDown = false;
  // prevent appending grids over each other
  // while (grid.hasChildNodes()) {
  //   grid.removeChild(grid.lastChild)
  // }
  constructGrid();

  //Color single cell if it's clicked
  grid.addEventListener("click", function (e) {
    e.preventDefault();
    setColor(e.target);
  });

  //Checking if left mouse is down or not
  grid.addEventListener("mousedown", function (e) {
    e.preventDefault();
    isMouseDown = e.button === 0 ? true : false
  });
  //Checking if is mouse released 
  document.addEventListener("mouseup", function (e) {
    e.preventDefault();
    isMouseDown = false;
  })
  //Checking if mouse is moving from cell to cell within the grid
  grid.addEventListener("mouseover", function (e) {
    if (isMouseDown) {
      setColor(e.target);
    }
  })

}
// building the grid
// how ? each iteration from the outer loop is for building the height of the grid one at a time 
// next in the inner loop after building a row which represents the height we're drawing the width of the grid 
// so that each row say its width is 5 then we're looping 5 times to build 5 pixels in one row
function constructGrid() {
  let h = height.value;
  let w = width.value;
  let row, column;
  for (let i = 0; i < h; i++) {
    // construct the height of grid
    row = document.createElement('tr');
    grid.appendChild(row);
    // construct grid's width
    for (let j = 0; j < w; j++) {
      column = document.createElement('td')
      row.appendChild(column)
    }
  }
}

function setColor(cell) {
  if (cell.nodeName === "TD") {
    cell.style.backgroundColor = myColor.value;
  }
}

// clearing  grid
document.getElementById("reset").addEventListener("click", () => clearGrid());

function clearGrid() {
  const cells = Array.from(grid.getElementsByTagName("td"));
  cells.forEach(cell => {
    if (cell.style.backgroundColor !== '') {
      cell.style.backgroundColor = '';
    }
  });
}

